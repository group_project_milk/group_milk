/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Provider;
import com.pnv.models.User;
import java.util.List;

/**
 *
 * @author linttd
 */
public interface userDao {
    public void saveOrUpdate(User user);

    public void delete(User user);

    public List<User> findAll();

    public User findByUserId(int idUser);

    public User findByUserCode(String Name);
}
