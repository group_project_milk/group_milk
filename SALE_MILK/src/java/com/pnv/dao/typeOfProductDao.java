/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Typeproduct;
import java.util.List;

/**
 *
 * @author linttd
 */
public interface typeOfProductDao {
    public void saveOrUpdate(Typeproduct typfOfProduct);

    public void delete(Typeproduct typfOfProduct);

    public List<Typeproduct> findAll();

    public Typeproduct findByTypeProductId(int IdTypeProduct);

    public Typeproduct findByTypeProductCode(String TypeProduct);
}
