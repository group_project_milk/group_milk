/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;


import com.pnv.models.Provider;
import java.util.List;

/**
 *
 * @author linttd
 */
public interface providerDao {
    public void saveOrUpdate(Provider provider);

    public void delete(Provider provider);

    public List<Provider> findAll();

    public Provider findByProviderId(int IdProvider);

    public Provider findByProviderCode(String NameOfCountry);
    
}
