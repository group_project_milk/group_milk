/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.api.controllers;

import com.pnv.dao.providerDao;
import com.pnv.models.Provider;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author linhnn
 */
@Controller
@RequestMapping(value = "api/provider")
public class ProviderApiController {
    @Autowired
    private providerDao provider_dao;

    @RequestMapping(method = RequestMethod.GET)
    public  @ResponseBody List<Provider> viewproviderPage() {
        /**
         * Get all titles
         */
        List<Provider> provider_list = provider_dao.findAll();
        return provider_list;
    }
    
    @RequestMapping(value = "{IdProvider}", method = RequestMethod.GET)
    public  @ResponseBody Provider getProviderByID(@PathVariable(value = "IdProvider") Integer IdProvider) {
        
//        Employees emp = new Employees();
//        emp.setEmpName("ddd");
//        emp.setAddress("ssss");
//        List<Employees> emp_lst = new ArrayList<Employees>();
        Provider provider = provider_dao.findByProviderId(IdProvider);
       // dep.setEmployees(emp_lst);
        return provider_dao.findByProviderId(IdProvider);
    }
}
