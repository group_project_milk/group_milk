/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.api.controllers;

import com.pnv.dao.productdao;
import com.pnv.dao.typeOfProductDao;
import com.pnv.models.Product;
import com.pnv.models.Typeproduct;
import com.pnv.utils.Constant;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author linttd
 */
@Controller
@RequestMapping(value = "api/product")
public class productApiController {
   
    @Autowired
    private productdao productDao;
    
    public productApiController() {}
    @RequestMapping(method = RequestMethod.GET )
     public @ResponseBody  List<Product> getProducts() {

        /**
         * Get all student
         */ 
        List<Product> product_list = productDao.findAll();
        
        return product_list;
    }
    
    
     @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {

        Product productForm = new Product();
        map.addAttribute("productForm", productForm);
        return "product_add";

    }
     
       @RequestMapping(value = "/add/{image}/{name}", method = RequestMethod.GET)
    public @ResponseBody  List<Product> AddNewBefore(@PathVariable(value = "image") String image,
                                @PathVariable(value = "name") String name, 
                                ModelMap map) {

        //Normally the parameter would be used to retrieve the object
        //In this case we keep it simple and return the name
         Product st = new Product();
        st.setImage(image);
        st.setName(name);
        map.put("productObject", st);
        productDao.saveOrUpdate(st);
        List<Product> product_list = productDao.findAll();        
        return product_list;
    }
       
       
       //xoa
        @RequestMapping(value = "/delete/{IdProduct}", method = RequestMethod.GET)
    public String doDeleteTitle(@PathVariable(value = "IdProduct") int IdProduct, ModelMap map) {

        productDao.delete(productDao.findByproductId(IdProduct));

        return Constant.REDIRECT + "/product";

    }
        
       
        
        //sua
        @RequestMapping(value = "/edit/{IdProduct}/{image}/{name}", method = RequestMethod.GET)
    public String viewEditStudentPage(@PathVariable(value = "IdProduct") int IdProduct,  
                                @PathVariable(value = "image") String image,
                                @PathVariable(value = "name") String name,
                                    ModelMap map) {

        Product productEdit = productDao.findByproductId(IdProduct);
        productEdit.setImage(image);
        productEdit.setName(name);
        productDao.saveOrUpdate(productEdit);
        map.addAttribute("productEdit", productEdit);
        return "resultEdit";
    }
}