/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.api.controllers;

import com.pnv.dao.typeOfProductDao;
import com.pnv.models.Typeproduct;
import com.pnv.utils.Constant;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author linhnn
 */
@Controller
@RequestMapping(value = "api/typeOfProduct")
public class TypeProductApiController {
   @Autowired
    private typeOfProductDao typeProduct_dao;

    public TypeProductApiController() {

    }

    @RequestMapping(method = RequestMethod.GET )
    public @ResponseBody    List<Typeproduct> getTypeProduct() {

        /**
         * Get all titles
         */ 
        List<Typeproduct> typeProduct_list = typeProduct_dao.findAll();
        
        return typeProduct_list;
    }

    @RequestMapping(value = "{IdTypeProduct}", method = RequestMethod.GET)
    public @ResponseBody    Typeproduct getTitleByID( @PathVariable(value = "IdTypeProduct") Integer IdTypeProduct) {        
        return typeProduct_dao.findByTypeProductId(IdTypeProduct);
    }
    
    
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String viewEditTitlePage(@RequestParam(value = "IdTypeProduct", required = true) int IdTypeProduct, ModelMap map) {

        Typeproduct typeProductForm = typeProduct_dao.findByTypeProductId(IdTypeProduct);
        map.addAttribute("typeProductForm", typeProductForm);
        return "typeOfProduct_addnew";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {

        Typeproduct typeOfProductForm = new Typeproduct();
        map.addAttribute("typeOfProductForm", typeOfProductForm);
        return "typeOfProduct_add";

    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String doAddNew(@Valid @ModelAttribute("typeOfProductForm") Typeproduct typeProductForm,
            BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            map.addAttribute("typeProductForm", typeProductForm);
            return "typeOfProduct_add";
        }
        typeProduct_dao.saveOrUpdate(typeProductForm);

        /**
         * Get all titles
         */
        List<Typeproduct> typeOfProduct_list = typeProduct_dao.findAll();
        map.put("typeOfProduct_list", typeOfProduct_list);
        map.addAttribute("add_success", "ok");

        return "typeOfProduct";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String doDeleteTitle(@RequestParam(value = "IdTypeProduct", required = true) int IdTypeProduct, ModelMap map) {

        typeProduct_dao.delete(typeProduct_dao.findByTypeProductId(IdTypeProduct));

        return Constant.REDIRECT + "/typeOfProduct";

    } 
}
