/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import com.pnv.dao.productdao;
import com.pnv.dao.typeOfProductDao;
import com.pnv.dao.userDaoImpl;
import com.pnv.models.Product;
import com.pnv.models.Typeproduct;
import com.pnv.models.User;
import com.pnv.utils.Constant;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author linttd
 */@Controller

public class IndexCotroller {
     @Autowired
     private productdao pro;
      userDaoImpl userDao;
     private typeOfProductDao type;
     
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String viewWelcomePage(ModelMap map) {
        map.addAttribute("user", new User());
         List<Product> product_list = pro.findAll();
         map.put("product_list", product_list);   
//         List<Typeproduct> typeproduct_list = type.findAll();
//         map.put("typeproduct_list", typeproduct_list);
        return "Login";
    }
    
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String viewWelcomePageIndex(ModelMap map) {
         List<Product> product_list = pro.findAll();
         map.put("product_list", product_list);   
//         List<Typeproduct> typeproduct_list = type.findAll();
//         map.put("typeproduct_list", typeproduct_list);
        return "index";
    }
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String doLogin(@ModelAttribute("user") User userForm,
            BindingResult result, ModelMap map) {
        map.put("user", userForm.getName().toString());

        List<User> user_list = new ArrayList<User>();
        userDao = new userDaoImpl();
        user_list = userDao.findAll();

        int count = 0;
        for (User i : user_list) {
            if (i.getName().equals(userForm.getName()) && i.getPassword().equals(userForm.getPassword())) {
                count++;
            }
        }
        if (count > 0) {
                return Constant.REDIRECT+"/index";
        } else {
//                Errors error = null;
//                error.rejectValue("password", "user.pass.wrong");
//                error.rejectValue("userName", "user.pass.wrong");
            return Constant.REDIRECT+"/";
        }




    }
    
    
}
