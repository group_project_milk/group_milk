/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import com.pnv.dao.providerDaoImpl;
import com.pnv.dao.typeOfProductDaoImpl;
import com.pnv.models.Provider;
import com.pnv.models.Typeproduct;
import com.pnv.utils.Constant;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author linttd
 */

@Controller
@RequestMapping(value = "/typeOfProduct")
public class typeOfProductController {
    @Autowired
    private typeOfProductDaoImpl typeProduct_dao = new typeOfProductDaoImpl();

    public typeOfProductController() {
    }
    @RequestMapping(method = RequestMethod.GET)
    public String viewproviderPage(ModelMap map) {
        /**
         * Get all titles
         */
        List<Typeproduct> typeProduct_list = typeProduct_dao.findAll();
        map.put("typeProduct_list", typeProduct_list);
        return "typeOfProduct";
    }
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String viewEditTitlePage(@RequestParam(value = "IdTypeProduct", required = true) int IdTypeProduct, ModelMap map) {

        Typeproduct TypeForm = typeProduct_dao.findByTypeProductId(IdTypeProduct);
        map.addAttribute("TypeForm", TypeForm);
        return "typeOfProduct_add";
    }
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {

        Typeproduct TypeForm = new Typeproduct();
        map.addAttribute("TypeForm", TypeForm);
        return "typeOfProduct_add";

    }
    
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String doAddNew(@Valid @ModelAttribute("TypeForm") Typeproduct TypeForm,
            BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            map.addAttribute("TypeForm", TypeForm);
            return "typeOfProduct_add";
        }
        typeProduct_dao.saveOrUpdate(TypeForm);

        /**
         * Get all titles
         */
        List<Typeproduct> typeProduct_list = typeProduct_dao.findAll();
        map.put("typeProduct_list", typeProduct_list);
        map.addAttribute("add_success", "ok");

        return "typeOfProduct";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String doDeleteTitle(@RequestParam(value = "IdTypeProduct", required = true) int IdTypeProduct, ModelMap map) {

        typeProduct_dao.delete(typeProduct_dao.findByTypeProductId(IdTypeProduct));

        return Constant.REDIRECT + "/typeOfProduct";

    }
}

