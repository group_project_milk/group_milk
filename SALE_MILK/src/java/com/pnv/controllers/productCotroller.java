package com.pnv.controllers;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import com.pnv.dao.productDaoImpl;
import com.pnv.dao.providerDaoImpl;
import com.pnv.dao.typeOfProductDaoImpl;
import com.pnv.models.Product;
import com.pnv.models.Provider;
import com.pnv.models.Typeproduct;
import com.pnv.utils.Constant;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author linttd
 */
@Controller
@RequestMapping(value = "/product")
public class productCotroller {
    @Autowired
    private productDaoImpl product_Dao;
    @Autowired
    private providerDaoImpl provider_Dao;
    @Autowired
    private typeOfProductDaoImpl typeProduct_Dao;
    public productCotroller() {
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public String viewproductPage(@RequestParam(value = "IdProvider", required = false) Integer IdProvider, @RequestParam(value = "IdTypeProduct", required = false) Integer IdTypeProduct, ModelMap map) {

        List<Product> product_list;

        if (IdProvider != null) {
            product_list = provider_Dao.findByProviderId(IdProvider).getProducts();
        } else if (IdTypeProduct != null) {
            product_list = typeProduct_Dao.findByTypeProductId(IdTypeProduct).getProducts();
        } else {
            product_list = product_Dao.findAll();
        }

        map.put("product_list", product_list);
        return "product";
    }
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {

        List<Provider> provider_lst = provider_Dao.findAll();
        map.addAttribute("provider_lst", provider_lst);
        List<Typeproduct> typeProductForm = typeProduct_Dao.findAll();
        map.addAttribute("typeProduct_lst", typeProductForm);

        map.addAttribute("productForm", new Product());

        return "product_add";

    }
    
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String doAddNew(@Valid @ModelAttribute("productForm") Product productForm,
            BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            List<Provider> provider_lst = provider_Dao.findAll();
            map.addAttribute("provider_lst", provider_lst);
            List<Typeproduct> typeProductForm = typeProduct_Dao.findAll();
            map.addAttribute("typeProduct_lst", typeProductForm);
            map.addAttribute("productForm", productForm);

            return "product_add";
        }

        product_Dao.saveOrUpdate(productForm);

        /**
         * Get all titles
         */
        List<Product> product_list = product_Dao.findAll();
        map.put("product_list", product_list);
        map.addAttribute("add_success", "ok");

        return "product";
    }
    
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String viewEditTitlePage(@RequestParam(value = "IdProduct", required = true) int IdProduct, ModelMap map) {
        List<Provider> provider_lst = provider_Dao.findAll();
        map.addAttribute("provider_lst", provider_lst);
        List<Typeproduct> typeProductForm = typeProduct_Dao.findAll();
        map.addAttribute("typeProduct_lst", typeProductForm);
        Product productForm = product_Dao.findByproductId(IdProduct);
        map.addAttribute("productForm", productForm);
        return "product_add";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String doDeleteTitle(@RequestParam(value = "IdProduct", required = true) int IdProduct, ModelMap map) {

        product_Dao.delete(product_Dao.findByproductId(IdProduct));

        return Constant.REDIRECT + "/product";

    }
}