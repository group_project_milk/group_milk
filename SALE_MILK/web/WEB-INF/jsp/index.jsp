<%@page import="com.pnv.models.Product"%>
<%@page import="java.util.List"%>
<%@page import="com.pnv.dao.productDaoImpl"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="./include.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="/resources/styles/styles.css" />" rel="stylesheet" type="text/css" />
        <title>Welcome to Spring Web MVC project</title>
    </head>

    <body>
        
        <div class="Frame">
            <div class="banner">
                <div class="logo">
                    <img src="resources/images/logo.png"/>
                </div>
                <div class="logo-right"><img src="resources/images/banner.png"/></div>
            </div>
            <div class="clear"></div>
            <!--Menu -->
            <div class="menu">
                <ul class="menu">
                     <li><a href="<%=request.getContextPath()%>/index">home</a></li>
                    <li><a href="<%=request.getContextPath()%>/product">product management</a></li>
                    <li><a href="<%=request.getContextPath()%>/provider">provider management</a></li>
                    <li><a href="<%=request.getContextPath()%>/typeOfProduct">type of product management</a></li>
                </ul>	
            </div>
            <div class="mainContent">
                <div class="slide">
                    <img src="resources/images/catalogeBanner.PNG"/>
                </div>
                <div class="content">
                    <div class="title">
                        <img src="resources/images/cow_sell_top.png"/>
                        <a>SẢN PHẨM CHO BÉ</a>
                    </div>
                   <%
                        productDaoImpl productdao = new productDaoImpl();
                        List<Product> product_list = productdao.findAll();
                        String page1 = "";
                        String page2 = "";
                        int start = 0;
                        int end;
                        if (product_list.size() < 16) {
                            end = product_list.size();
                        } else {
                            end = 8;
                        }
                        if (request.getParameter("start") != null) {
                            page1 = request.getParameter("start");
                            start = Integer.parseInt(page1);
                        }
                        if (request.getParameter("end") != null) {
                            page2 = request.getParameter("end");
                            end = Integer.parseInt(page2);
                        }
                        List<Product> list = productdao.getSub(product_list, start, end);
                    %>


                    <%
                        int limit = product_list.size() / 8;
                        if (limit * 8 < product_list.size()) {
                            limit += 1;
                        }
                        for (int i = 1; i <= limit; i++) {
                            int a = (i - 1) * 8;
                            int b = i * 8;
                            if (b > product_list.size()) {
                                b = product_list.size();
                            }
                    %>
                    <a href="?start=<%=a%>&end=<%=b%>"><%=i%></a>
                    <%
                        }
                    %>
                    <div>
                        <ul class="listImage">
                            <%int dem = 0; %>
                            <c:forEach items="${product_list}" var="product">
                                <% dem ++;
                                if(dem <= 4) {
                                    
                                
                                   %>
                            <li>
                            <img src="resources/<c:out value="${product.image}"/> " />
                            
<!--                                <img src ="resources/images/meItem1.PNG"/><br/> -->
                                <a><c:out value="${product.name}"/></a><br/>
                                Price: <span><c:out value="${product.price}"/></span><br/>
                                Quantity:<span><c:out value="${product.quantity}"/></span><br/>
                                Production date:<span><c:out value="${product.productionDate}"/></span><br/>
                                Expire_date  <span><c:out value="${product.expireDate}"/></span><br/>
                                        
                            </li>
                            <% 
                                }
                                
                                   %>
                            </c:forEach>
                        </ul>
                    </div>
                </div>
                <div style="margin-top: 10px;"></div>
                <div class="content">
                    <div class="title">
                        <img src="resources/images/cow_sell_top.png"/>
                        <a>SẢN PHẨM CHO MẸ</a>
                    </div>
                    <div>
                        <ul class="listImage">
                            <% dem = 0; %>
                            <c:forEach items="${product_list}" var="product">
                                <% dem ++;
                                if(dem > 4 && dem <= 8) {
                                    
                                
                                   %>
                            <li>
                            <img src="resources/<c:out value="${product.image}"/> " />
                            
<!--                                <img src ="resources/images/meItem1.PNG"/><br/> -->
                                <a><c:out value="${product.name}"/></a><br/>
                                Price: <span><c:out value="${product.price}"/></span><br/>
                                Quantity:<span><c:out value="${product.quantity}"/></span><br/>
                                Production date:<span><c:out value="${product.productionDate}"/></span><br/>
                                Expire_date  <span><c:out value="${product.expireDate}"/></span><br/>
                                        
                            </li>
                            <% 
                                }
                                
                                   %>
                            </c:forEach>
                        </ul>
                    </div>
                </div>
                <div style="margin-top: 10px;"></div>
                <div class="content">
                    <div class="title">
                        <img src="resources/images/cow_sell_top.png"/>
                        <a>SẢN PHẨM CHO NGƯỜI LỚN</a>
                    </div>
                    <div>
                        <ul class="listImage">
                            <% dem = 0; %>
                            <c:forEach items="${product_list}" var="product">
                                <% dem ++;
                                if(dem > 8 && dem <= 12) {
                                    
                                
                                   %>
                            <li>
                            <img src="resources/<c:out value="${product.image}"/> " />
                            
<!--                                <img src ="resources/images/meItem1.PNG"/><br/> -->
                                <a><c:out value="${product.name}"/></a><br/>
                                Price: <span><c:out value="${product.price}"/></span><br/>
                                Quantity:<span><c:out value="${product.quantity}"/></span><br/>
                                Production date:<span><c:out value="${product.productionDate}"/></span><br/>
                                Expire_date  <span><c:out value="${product.expireDate}"/></span><br/>
                                        
                            </li>
                            <% 
                                }
                                
                                   %>
                            </c:forEach>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer">
                <a>Alberomilk chấp nhận các loại thẻ</a>
                <div>
                    <ul class="menu_footer">
                        <li>
                            <a href="#"><img src="resources/images/bank_1.png"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/bank_2.png"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/bank_3.png"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/bank_4.png"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/bank_5.png"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/bank_10.png"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/bank_6.png"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/bank_8.png"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/bank_9.png"/></a>
                        </li>
                    </ul>
                </div>
                <div>
                    <ul class="menu_social">
                        <li>
                            <a href="#"><img src="resources/images/icon_facebook.png"/><a/>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/icon_flickr.png"/><a/>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/icon_google.png"/></a>
                        </li>
                        <li>
                            <a href="#"> <img src="resources/images/icons_youtube.png"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/icon_pinterest.png"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/icon_twitter.png"/></a>
                        </li>
                    </ul>
                </div>
                <p>Design by: Tran Thi Duc Lin</p>
            </div>
        </div>
    </body>
    <div id="arrows">
			<div>
				<a href=""><img src="resources/images/Arrow_Up.png" title="Lên đầu trang"/></a>
				<a href="#footer"><img src="resources/images/arrow_down.png" title="Xuống cuối trang"/></a>
			</div>
		</div>
</html>
