<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="./include.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="/resources/styles/styles.css" />" rel="stylesheet" type="text/css" />
        <title>Welcome to Spring Web MVC project</title>
    </head>

    <body>
        <div class="Frame">
            <div class="banner">
                <div class="logo">
                    <img src="<c:url value="/resources/images/logo.png" />" />
                </div>
                <div class="logo-right"><img src="<c:url value="/resources/images/banner.png" />"/></div>
            </div>
            <div class="clear"></div>
            <!--Menu -->
            <div class="menu">
                <ul class="menu">
                    <li><a href="<%=request.getContextPath()%>/index">home</a></li>
                    <li><a href="<%=request.getContextPath()%>/product">product management</a></li>
                    <li><a href="<%=request.getContextPath()%>/provider">provider management</a></li>
                    <li><a href="<%=request.getContextPath()%>/typeOfProduct">type of product management</a></li>
                </ul>	
            </div>
            <div class="mainContent">
                <div class="slide">
                    <img src="<c:url value="/resources/images/catalogeBanner.PNG" />"/>
                </div>

                <div class="content">
                    <div class="title">
                        <img src="<c:url value="/resources/images/cow_sell_top.png" />"/>
                        <a>PROVIDER MANAGEMENT</a>
                    </div>
                    <a href="#"><img class="image" src="<c:url value="/resources/images/daily1.jpg" />"/></a>
                    <a href="#"><img class="image" src="<c:url value="/resources/images/daily2.jpg" />"/></a>
                    <p class="text">Management of supplier information and may view edit delete information of technological proxies also granted</p>

                    
                        <h3>${add_success}</h3>
                        <a href="<%=request.getContextPath()%>/provider/add" class="button">Add New</a>
                        <div class="border">
                            <table>
                                <tr>
                                    <th>ID provider</th>
                                    <th>Name of country</th>
                                    <th>Action</th>

                                    <c:forEach items="${provider_list}" var="provider">  
                                    <tr>  
                                        <td><c:out value="${provider.idProvider}"/></td>  
                                        <td><c:out value="${provider.nameOfCountry}"/></td>  


                                        <td align="center"><a href="<%=request.getContextPath()%>/provider/edit?IdProvider=${provider.idProvider}">Edit</a> | <a href="<%=request.getContextPath()%>/provider/delete?IdProvider=${provider.idProvider}">Delete</a></td>  

                                    </tr>  
                                </c:forEach> 
                                </tr>

                            </table>
                        </div>
                    
                </div>
            </div>
            <div class="footer">
                <a>Alberomilk chấp nhận các loại thẻ</a>
                <div>
                    <ul class="menu_footer">
                         <li>
                            <a href="#"><img src=" <c:url value="/resources/images/bank_1.png" />"/></a>

                        </li>
                        <li>
                            <a href="#"><img src=" <c:url value="/resources/images/bank_2.png" />"/></a>
                        </li>
                        <li>
                            <a href="#"><img src=" <c:url value="/resources/images/bank_3.png" />"/></a>
                        </li>
                        <li>
                            <a href="#"><img src=" <c:url value="/resources/images/bank_4.png" />"/></a>
                        </li>
                        <li>
                            <a href="#"><img src=" <c:url value="/resources/images/bank_5.png" />"/></a>
                        </li>
                        <li>
                            <a href="#"><img src=" <c:url value="/resources/images/bank_10.png" />"/></a>
                        </li>
                        <li>
                            <a href="#"><img src=" <c:url value="/resources/images/bank_6.png" />"/></a>
                        </li>
                        <li>
                            <a href="#"><img src=" <c:url value="/resources/images/bank_8.png" />"/></a>
                        </li>
                        <li>
                            <a href="#"><img src=" <c:url value="/resources/images/bank_9.png" />"/></a>
                        </li>
                    </ul>
                </div>
                <div>
                    <ul class="menu_social">
                        <li>
                            <a href="#"><img src="<c:url value="/resources/images/icon_facebook.png" />"/><a/>
                        </li>
                        <li>
                            <a href="#"><img src="<c:url value="/resources/images/icon_flickr.png" />"/><a/>
                        </li>
                        <li>
                            <a href="#"><img src="<c:url value="/resources/images/icon_google.png" />"/></a>
                        </li>
                        <li>
                            <a href="#"> <img src="<c:url value="/resources/images/icons_youtube.png" />"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="<c:url value="/resources/images/icon_pinterest.png" />"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="<c:url value="/resources/images/icon_twitter.png" />"/></a>
                        </li>
                    </ul>
                </div>
                <p>Design by: Tran Thi Duc Lin</p>
            </div>
        </div>
    </body>
    <div id="arrows">
        <div>

            <a href=""><img src="<c:url value="/resources/images/Arrow_Up.png" />"/></a>
            <a href="#footer"><img src="<c:url value="/resources/images/arrow_down.png" />"/></a>
        </div>
    </div>
</html>
