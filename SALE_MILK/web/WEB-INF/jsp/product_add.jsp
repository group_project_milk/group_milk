<%-- 
    Document   : product_add
    Created on : May 18, 2015, 4:29:24 PM
    Author     : linttd
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="./include.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="/resources/styles/styles.css" />" rel="stylesheet" type="text/css" />
        <title>Welcome to Spring Web MVC project</title>
    </head>

    <body>
        <div class="Frame">
            <div class="banner">
                <div class="logo">
                    <img src="<c:url value="/resources/images/logo.png" />" />

                </div>
                <div class="logo-right"><img src="<c:url value="/resources/images/banner.png" />" /></div>

            </div>
            <div class="clear"></div>
            <!--Menu -->
            <div class="menu">
                <ul class="menu">
                   <li><a href="<%=request.getContextPath()%>/index">home</a></li>
                    <li><a href="<%=request.getContextPath()%>/product">product management</a></li>
                    <li><a href="<%=request.getContextPath()%>/provider">provider management</a></li>
                    <li><a href="<%=request.getContextPath()%>/typeOfProduct">type of product management</a></li>
                </ul>	
            </div>
            <div class="mainContent">
                <div class="slide">
                    <img src="<c:url value="/resources/images/catalogeBanner.PNG" />"/>

                </div>

                <div class="content">
                    <div class="title">
                        <img src="<c:url value="/resources/images/cow_sell_top.png" />"/>

                        <a>PROVIDER MANAGEMENT</a> 
                    </div>
                    <a href="#"><img class="image" src="<c:url value="/resources/images/daily1.jpg" />"/></a>
                    <a href="#"><img class="image" src="<c:url value="/resources/images/daily2.jpg" />"/></a>
                    <p class="text">Change information of provider</p>
                    <div align="center">
                        <center>
                            <table border="0" width="90%">
                                <form:form action="add" modelAttribute="productForm" method="POST" >
                                    <form:hidden path="IdProduct" />
                                    <tr>
                                        <td>Image: </td>
                                        <td><form:input path="Image" size="30"/></td>
                                        <td><form:errors path="Image" cssClass="error"/></td>
                                    </tr>
                                    <tr>
                                        <td>Name: </td>
                                        <td><form:input path="Name" size="30"/></td>
                                        <td><form:errors path="Name" cssClass="error"/></td>
                                    </tr>
                                    <tr>
                                        <td>Price: </td>
                                        <td><form:input path="price" size="30"/></td>
                                        <td><form:errors path="price" cssClass="error"/></td>
                                    </tr>
                                    <tr>
                                        <td>Quantity: </td>
                                        <td><form:input path="quantity" size="30"/></td>
                                        <td><form:errors path="quantity" cssClass="error"/></td>
                                    </tr>
                                    <tr>
                                        <td>Production Date: </td>
                                        <td><form:input path="productionDate" size="30"/></td>
                                        <td><form:errors path="productionDate" cssClass="error"/></td>
                                    </tr>
                                    <tr>
                                        <td>Expire Date: </td>
                                        <td><form:input path="expireDate" size="30"/></td>
                                        <td><form:errors path="expireDate" cssClass="error"/></td>
                                    </tr>
                                    <tr>
                                        <td>Provider :  </td>
                                        <td><form:select path="provider.IdProvider">
                                                <form:option value="" label="--Please Select"/>
                                                <form:options items="${provider_lst}" itemValue="IdProvider" itemLabel="NameOfCountry"/>
                                            </form:select></td>
                                        <td><form:errors path="provider.IdProvider" cssClass="error"/></td>
                                    </tr>

                                    <tr>
                                        <td>Type Of Product  </td>
                                        <td><form:select path="typeproduct.IdTypeProduct">
                                                <form:option value="" label="--Please Select"/>
                                                <form:options items="${typeProduct_lst}" itemValue="IdTypeProduct" itemLabel="TypeProduct"/>
                                            </form:select></td>
                                        <td><form:errors path="typeproduct.IdTypeProduct" cssClass="error"/></td>
                                    </tr>

                                    <tr>
                                        <td><a href="<%=request.getContextPath()%>/product" class="button">Back</a></td>
                                        <td align="center"><input type="submit" value="submit"/></td>
                                        <td></td>
                                    </tr>
                                </form:form>
                            </table>
                        </center>
                    </div>
                </div>
            </div>
            <div class="footer">
                <a>Alberomilk chấp nhận các loại thẻ</a>
                <div>
                    <ul class="menu_footer">
                        <li>
                            <a href="#"><img src=" <c:url value="/resources/images/bank_1.png" />"/></a>

                        </li>
                        <li>
                            <a href="#"><img src=" <c:url value="/resources/images/bank_2.png" />"/></a>
                        </li>
                        <li>
                            <a href="#"><img src=" <c:url value="/resources/images/bank_3.png" />"/></a>
                        </li>
                        <li>
                            <a href="#"><img src=" <c:url value="/resources/images/bank_4.png" />"/></a>
                        </li>
                        <li>
                            <a href="#"><img src=" <c:url value="/resources/images/bank_5.png" />"/></a>
                        </li>
                        <li>
                            <a href="#"><img src=" <c:url value="/resources/images/bank_10.png" />"/></a>
                        </li>
                        <li>
                            <a href="#"><img src=" <c:url value="/resources/images/bank_6.png" />"/></a>
                        </li>
                        <li>
                            <a href="#"><img src=" <c:url value="/resources/images/bank_8.png" />"/></a>
                        </li>
                        <li>
                            <a href="#"><img src=" <c:url value="/resources/images/bank_9.png" />"/></a>
                        </li>
                    </ul>
                </div>
                <div>
                    <ul class="menu_social">
                        <li>
                            <a href="#"><img src="<c:url value="/resources/images/icon_facebook.png" />"/><a/>
                        </li>
                        <li>
                            <a href="#"><img src="<c:url value="/resources/images/icon_flickr.png" />"/><a/>
                        </li>
                        <li>
                            <a href="#"><img src="<c:url value="/resources/images/icon_google.png" />"/></a>
                        </li>
                        <li>
                            <a href="#"> <img src="<c:url value="/resources/images/icons_youtube.png" />"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="<c:url value="/resources/images/icon_pinterest.png" />"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="<c:url value="/resources/images/icon_twitter.png" />"/></a>
                        </li>
                    </ul>
                </div>
                <p>Design by: Tran Thi Duc Lin</p>
            </div>
        </div>
    </body>
    <div id="arrows">
        <div>

            <a href=""><img src="<c:url value="/resources/images/Arrow_Up.png" />"/></a>
            <a href="#footer"><img src="<c:url value="/resources/images/arrow_down.png" />"/></a>
        </div>
    </div>
</html>

