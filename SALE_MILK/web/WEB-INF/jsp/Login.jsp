<%-- 
    Document   : Login
    Created on : May 21, 2015, 9:55:25 AM
    Author     : linttd
--%>
<%@ include file="./include.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="/resources/styles/CssRegister.css" />" rel="stylesheet" type="text/css" />
        <title>JSP Page</title>
    </head>
    <body>
        <div class="khungRe">
            <div id="login-box-4">
                <table border="0" width="90%">
                    <a href="#" title="Close" class="close">x</a>
                        <div class="sub-title">LOGIN<img src="resources/images/g.gif" height="30px"/></div>
                        <hr>
                    <form:form action="login" modelAttribute="user" method="POST" >
                        <tr>
                            <td align="left">User: </td>
                            <td align="left"><form:input path="name" size="30"/></td>
                            <td align="left"><form:errors path="name" cssClass="error"/></td>
                        </tr>
                        <tr>
                            <td>Password: </td>
                            <td><form:password path="password" size="30"/></td>
                            <td><form:errors path="password" cssClass="error"/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input class="btn" type="submit"/><input class="btn" type="Reset"/></td>
                            <td></td>
                        </tr>
                    </form:form>
                </table>
            </div>
        </div>
    </body>
</html>
